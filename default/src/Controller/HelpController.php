<?php

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Help controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class HelpController extends AppController
{

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     */
    public function index()
    {
        $this->set(compact('page', 'subpage'));
        return $this->render();
    }
}
